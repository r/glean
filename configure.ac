dnl -*- Autoconf -*-

AC_INIT([glean],
        [0.1],
        [alex.sassmannshausen@gmail.com],
        [glean],
        [www.glean.eu])

AC_SUBST(HVERSION, "\"0.1\"")
AC_SUBST(AUTHOR, "\"Alex Sassmannshausen\"")
AC_SUBST(COPYRIGHT, "'(2014)")
AC_SUBST(LICENSE, agpl3+)

AC_CONFIG_SRCDIR(glean)
AC_CONFIG_AUX_DIR([build-aux])
AM_INIT_AUTOMAKE([1.12 gnu silent-rules subdir-objects color-tests parallel-tests -Woverride -Wno-portability])
AM_SILENT_RULES([yes])

AC_CONFIG_FILES([Makefile])
AC_CONFIG_FILES([po/Makefile.in])
AC_CONFIG_FILES([pre-inst-env], [chmod +x pre-inst-env])
AC_CONFIG_FILES([test-env], [chmod +x test-env])
AC_CONFIG_FILES([scripts/glean],[chmod +x scripts/glean])
AC_CONFIG_FILES([glean/config.scm])

AM_GNU_GETTEXT([external])

dnl Search for 'guile' and 'guild'.  This macro defines
dnl 'GUILE_EFFECTIVE_VERSION'.
GUILE_PKG([3.0 2.2 2.0])
GUILE_PROGS
GUILE_SITE_DIR
if test "x$GUILD" = "x"; then
   AC_MSG_ERROR(['guild' binary not found; please check your guile-2.x installation.])
fi

dnl Check whether Artanis is installed; install otherwise.
dnl GLEAN_CHECK_ARTANIS
dnl AM_CONDITIONAL([INSTALL_ARTANIS],
dnl                [test "x$ac_cv_glean_artanis_installed" = xno])
dnl For now we consider the web client experimental and don't even
dnl check whether artanis is available.
dnl GUILE_MODULE_REQUIRED([artanis artanis])

dnl Locate libgcrypt & test it.
AC_ARG_WITH([libgcrypt-prefix],
  [AS_HELP_STRING([--with-libgcrypt-prefix=DIR], [search for GNU libgcrypt in DIR])],
  [case "$withval" in
    yes|no)
      LIBGCRYPT="libgcrypt"
      LIBGCRYPT_PREFIX="no"
      ;;
    *)
      LIBGCRYPT="$withval/lib/libgcrypt"
      LIBGCRYPT_PREFIX="$withval"
      ;;
   esac],
  [LIBGCRYPT="libgcrypt"])

dnl Library name suitable for `dynamic-link'.
AC_MSG_CHECKING([for libgcrypt shared library name])
AC_MSG_RESULT([$LIBGCRYPT])
AC_SUBST([LIBGCRYPT])
AC_SUBST([LIBGCRYPT_PREFIX])


dnl Installation directories for .scm and .go files.
guilemoduledir="${datarootdir}/guile/site/$GUILE_EFFECTIVE_VERSION"
guileobjectdir="${libdir}/guile/$GUILE_EFFECTIVE_VERSION/site-ccache"
AC_SUBST([guilemoduledir])
AC_SUBST([guileobjectdir])

AC_OUTPUT
